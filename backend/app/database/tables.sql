-- Image table
CREATE TABLE IF NOT EXISTS image
 (id INTEGER PRIMARY KEY,
   path varchar(64) NOT NULL);

-- MediaFile
 CREATE TABLE IF NOT EXISTS media_file
 (id INTEGER PRIMARY KEY,
  name varchar(20) NOT NULL,
  path varchar(64) NOT NULL UNIQUE,
  image_id INTEGER,
  FOREIGN KEY (image_id)
    REFERENCES image(id));

-- Playlist
CREATE TABLE IF NOT EXISTS playlist
 (id INTEGER PRIMARY KEY,
 name varchar(20) NOT NULL);

-- Media-Playlist link
CREATE TABLE IF NOT EXISTS media_playlist
 (media_id INTEGER NOT NULL,
  playlist_id INTEGER NOT NULL,
  FOREIGN KEY (media_id)
    REFERENCES media_file(id),
  FOREIGN KEY (playlist_id)
    REFERENCES playlist(id)
  CONSTRAINT unq UNIQUE (media_id, playlist_id));

-- Tag
 CREATE TABLE IF NOT EXISTS tag
  (id INTEGER PRIMARY KEY,
  name varchar(20) NOT NULL);

-- Media-Tag link
 CREATE TABLE IF NOT EXISTS media_tag
  (media_id INTEGER NOT NULL,
   tag_id INTEGER NOT NULL,
   FOREIGN KEY (media_id)
     REFERENCES media_file(id),
   FOREIGN KEY (tag_id)
     REFERENCES tag(id)
   CONSTRAINT unq UNIQUE (media_id, tag_id));

-- Category
CREATE TABLE IF NOT EXISTS category
(id INTEGER PRIMARY KEY,
name varchar(20) NOT NULL UNIQUE);

-- Media-Category link
 CREATE TABLE IF NOT EXISTS media_category
  (
   media_id INTEGER NOT NULL,
   category_id INTEGER NOT NULL,
   FOREIGN KEY (media_id)
     REFERENCES media_file(id),
   FOREIGN KEY (category_id)
     REFERENCES category(id)
   CONSTRAINT unq UNIQUE (media_id, category_id));
