import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormControl, Validators} from '@angular/forms';

export interface MusicDialogData {
  name: string;
  file: string;
}

@Component({
  selector: 'app-add-music-dialog',
  templateUrl: './add-music-dialog.component.html',
  styleUrls: ['./add-music-dialog.component.scss']
})
export class AddMusicDialogComponent implements OnInit {
  nameFormControl = new FormControl('', [
      Validators.required,
  ]);
  fileControl =  new FormControl('', [
      Validators.required,
  ]);
  fileTypes = '.mp3, .wav, .flac, .mp4, .ogg';

  constructor(
    public dialogRef: MatDialogRef<AddMusicDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: MusicDialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
  }

}
