from sqlite3 import IntegrityError
from typing import List
from pathlib import Path
import aiofiles
import uuid

import fastapi
from fastapi import HTTPException, Response, UploadFile, File
from fastapi.responses import FileResponse, StreamingResponse

from database.database import Database
from models.Category import Category
from models.MediaFile import MediaFile
from models.Playlist import Playlist
from models.Tag import Tag

router = fastapi.APIRouter(prefix="/api/media_file", tags=["MediaFile"])
database: Database = Database("data/database.sqlite")


@router.post("", response_model=MediaFile)
async def upload_new_media_file(name: str, file: UploadFile = File(...)):
    """
    Upload a new MediaFile
    """

    path = Path(f"./data/files/{uuid.uuid4()}")
    if path.exists():
        raise HTTPException(status_code=409, detail="File with that name already exists.")

    async with aiofiles.open(path, "wb") as f:
        await f.write(await file.read())

    return database.insert_media_file(name, path)


@router.post("/{media_file_id}/image", response_model=MediaFile)
async def upload_new_media_file_image(media_file_id: int, file: UploadFile = File(...)):
    """
    Upload a new MediaFile
    """
    path = Path(f"./data/images/{uuid.uuid4()}")
    if path.exists():
        raise HTTPException(status_code=409, detail="File with that name already exists.")

    async with aiofiles.open(path, "wb") as f:
        await f.write(await file.read())

    return database.insert_media_file_image(media_file_id, path)


@router.get("/{media_file_id}/image")
async def get_media_file_image(media_file_id: int) -> FileResponse:
    """
    Add a category to a exist
    """
    image = database.get_image_of_media(media_file_id)
    if not image:
        raise HTTPException(status_code=404, detail="File not found")

    return FileResponse(image)


@router.post("/{media_file_id}/tag")
async def add_tag_to_media_file(media_file_id: int, tag: str):
    """
    Add a tag to a MediaFile
    """
    database.add_tag_to_media_file(tag, media_file_id)


@router.get("", response_model=List[MediaFile])
async def get_all_media_files(page: int, items_per_page: int = 16):
    """
    Get a paginated list of all MediaFiles
    """
    results = database.get_all_media_files(page, items_per_page)
    if results or page == 0:  # Allow page 0 to return none if there are no items
        return results
    else:
        raise HTTPException(status_code=404, detail="Page out of range")


@router.get("/{media_file_id}", response_model=MediaFile)
async def get_media_file(media_file_id: int) -> MediaFile:
    result = database.get_media_file(media_file_id)
    if not result:
        raise HTTPException(status_code=404, detail="MediaFile not found")
    else:
        return result


@router.get("/{media_file_id}/download")
async def get_media_file_download(media_file_id: int) -> FileResponse:
    media_file = database.get_media_file(media_file_id)
    if not media_file:
        raise HTTPException(status_code=404, detail="File not found")

    return FileResponse(str(media_file.path))


@router.get("/{media_file_id}/stream")
async def get_media_file_stream(media_file_id: int) -> StreamingResponse:
    media_file = database.get_media_file(media_file_id)
    if not media_file:
        raise HTTPException(status_code=404, detail="File not found")

    return StreamingResponse(open(media_file.path, mode="rb"))


@router.get("/{media_file_id}/category", response_model=List[Category])
async def get_media_file_categories(media_file_id: int):
    return database.get_categories_of_media_file(media_file_id)


@router.get("/{media_file_id}/tag", response_model=List[Tag])
async def get_media_file_tags(media_file_id: int):
    """
    Get all tags assigned to media file
    """
    return database.get_tags_of_media_file(media_file_id)


@router.get("/{media_file_id}/playlist", response_model=List[Playlist])
async def get_media_file_playlist(media_file_id: int):
    """
    Get the playlists this MediaFile features in
    """
    raise HTTPException(status_code=500, detail="Not Implemented")


@router.delete("/{media_file_id}")
async def delete_media_file(media_file_id: int):
    database.delete_media_file(media_file_id)
