import os
import sqlite3
from pathlib import Path
from typing import List, Iterable, Optional, Tuple, Union

from models.Category import Category
from models.MediaFile import MediaFile
from models.Playlist import Playlist
from models.Tag import Tag


class Database:
    # TODO: Make Singleton
    def __init__(self, connection_string: str = "data/database.sqlite"):
        self._con = sqlite3.connect(connection_string)
        Database.instance = self
        self._ensure_created()

    def _ensure_created(self):
        """
        Load and execute the .sql tables file. This will ensure that all the necessary tables are created.
        """
        with self._con:
            c = self._con.cursor()

            # Open and execute .sql to create tables.
            with open("./database/tables.sql") as f:
                c.executescript(f.read())

        self._con.commit()

    def insert_media_file(self, name: str, path: Path) -> MediaFile:
        if not name:
            raise ValueError("No Name was provided.")
        with self._con as conn:
            cur = conn.cursor()

            cur.execute("""
                INSERT INTO media_file (name, path)
                VALUES (?, ?);
            """, (name, str(path)))

            return MediaFile(
                id=cur.lastrowid,
                name=name,
                path=path,
                image=None,
            )

    def get_media_file(self, id: int) -> Optional[MediaFile]:
        """
        Try to find and return a MediaFile from the database.
        If not found it will return None
        :param id: unique ID to search for.
        :return: MediaFile or None
        """
        with self._con as conn:
            cur = conn.cursor()
            cur.execute("""
                SELECT
                    media_file.id,
                    media_file.name,
                    media_file.path,
                    image.path
                FROM
                    media_file
                    LEFT JOIN image on media_file.image_id = image.id
                    
                WHERE media_file.id == (?)
            """, (id,))
            result = cur.fetchone()

        if result:
            return MediaFile(
                id=result[0],
                name=result[1],
                path=result[2],
                image=result[3],
            )

    def get_all_media_files(self, page: int, items_per_page: int = 16) -> Tuple[MediaFile]:
        """
        Get a generator object that represents a paginated query. Each iteration will be a new page,
        leading all the way up until none is left, in which a StopIteration Exception will be throw.

        :param page: 0-indexed
        :param items_per_page:
        :return:
        """
        with self._con as conn:
            curr = conn.cursor()
            curr.execute("""
                SELECT
                    media_file.id,
                    media_file.name,
                    media_file.path,
                    image.id
                FROM
                    media_file
                    LEFT JOIN image on media_file.image_id = image.id
                ORDER BY media_file.id
                LIMIT (?)
                OFFSET (?);
            """, (items_per_page, page * items_per_page))
            results = curr.fetchall()
            if results:
                return tuple(
                    MediaFile(
                        id=result[0],
                        name=result[1],
                        path=result[2],
                        image=result[3],
                    ) for result in results)

    def get_media_files_from_tag(self, tag: str, items_per_page: int = 16) -> Iterable[Tuple[MediaFile]]:
        ...

    def get_media_files_from_category(self, category: int, items_per_page: int = 16) -> Iterable[
        Tuple[MediaFile]]:
        ...

    def get_playlist(self, playlist_id: int) -> Playlist:
        with self._con as conn:
            cur = conn.cursor()
            cur.execute("""
                SELECT
                    *
                FROM
                    playlist
                WHERE id == (?)
            """, (playlist_id,))

            result = cur.fetchone()
            return Playlist(
                id=result[0],
                name=result[1],
            )

    def get_media_files_from_playlist(self, playlist_id: int) -> Tuple[MediaFile]:
        with self._con as conn:
            curr = conn.cursor()
            while True:
                curr.execute("""
                            SELECT
                                media_id
                            FROM
                                media_playlist
                            WHERE playlist_id = (?)
                            ORDER BY media_id;
                        """, (playlist_id,))

                results = []
                for result in curr.fetchall():
                    curr.execute("""
                        SELECT
                            media_file.id,
                            media_file.name,
                            media_file.path,
                            image.id
                        FROM
                            media_file
                            LEFT JOIN image on media_file.image_id = image.id

                        WHERE media_file.id == (?)                
                    """, (result[0],))
                    results.append(curr.fetchone())

                return tuple(
                    MediaFile(
                        id=result[0],
                        name=result[1],
                        path=result[2],
                        image=result[3],
                    ) for result in results)

    def add_category_to_media_file(self, category: int, media_file: Union[MediaFile, int]):
        """
        Attach a category to a media_file
        :param category: The id of the category or Enum category
        :param media_file: The MediaFile object or the id of the entry
        """

        with self._con as conn:
            cur = conn.cursor()
            cur.execute("""
                INSERT INTO media_category (media_id, category_id)
                VALUES (?, ?);
            """, (
                media_file.id if isinstance(media_file, MediaFile) else media_file,
                category
            ))

    def get_categories_of_media_file(self, media_file: Union[MediaFile, int]) -> List[Category]:
        """
        Get a list of the categories for the MediaFile
        :param media_file:
        :return:
        """
        with self._con as conn:
            cur = conn.cursor()
            cur.execute("""
                SELECT category_id
                FROM media_category
                WHERE media_id == (?)
            """, (media_file.id if isinstance(media_file, MediaFile) else media_file,))
            return [Category(id=result[0], name=name[1]) for result in cur.fetchall()]

    def add_tag_to_media_file(self, tag: str, media_file: Union[MediaFile, int]):
        """
        Due to tags being user defined this will check if the tag exists and grab the Id to be used
        in the link table, or create a new one to be used.
        :param tag:
        :param media_file:
        :return:
        """
        # TODO: reuse existing tags
        with self._con as conn:
            cur = conn.cursor()
            cur.execute("""
                INSERT OR IGNORE INTO tag (name)
                VALUES (?)
            """, (tag,))
            tag_id = cur.lastrowid

            cur.execute("""
                INSERT INTO media_tag (media_id, tag_id) 
                VALUES (?, ?)
            """, (media_file.id if isinstance(media_file, MediaFile) else media_file, tag_id))

    def get_tags_of_media_file(self, media_file: Union[MediaFile, int]) -> List[Tag]:
        with self._con as conn:
            cur = conn.cursor()
            cur.execute("""
                SELECT tag_id
                FROM media_tag
                WHERE media_id = (?)
            """, (media_file.id if isinstance(media_file, MediaFile) else media_file,))

            results = []
            for result in cur.fetchall():
                cur.execute("""
                    SELECT name
                    FROM tag
                    WHERE id = (?)
                """, result)
                result = cur.fetchone()[0]
                results.append(Tag(id=cur.lastrowid, name=result))
            return results

    def insert_playlist(self, name) -> Playlist:
        with self._con as conn:
            cur = conn.cursor()
            cur.execute("""
                INSERT INTO playlist (name)
                VALUES (?)
            """, (name,))
            return Playlist(
                id=cur.lastrowid,
                name=name,
            )

    def add_media_file_to_playlist(self, playlist: Union[Playlist, int], media_file: Union[MediaFile, int]):
        with self._con as conn:
            cur = conn.cursor()
            cur.execute("""
                INSERT INTO media_playlist (media_id, playlist_id)
                VALUES (?, ?)
            """, (
                media_file.id if isinstance(media_file, MediaFile) else media_file,
                playlist.id if isinstance(playlist, Playlist) else playlist,
            ))

    def delete_media_file(self, _id):
        with self._con as conn:
            cur = conn.cursor()

            cur.execute("""
                SELECT path
                FROM media_file
                WHERE id = (?)
            """, (_id,))

            path = cur.fetchone()[0]
            try:
                os.remove(path)
            except OSError as e:
                print(e)

            cur.execute("""
                DELETE FROM media_file
                WHERE id = (?)
            """, (_id,))
            cur.execute("""
                DELETE FROM media_playlist
                WHERE media_id = (?)
            """, (_id,))
            cur.execute("""
                DELETE FROM media_tag
                WHERE media_id = (?)
            """, (_id,))
            cur.execute("""
                DELETE FROM media_category
                WHERE media_id = (?)
            """, (_id,))

    def get_playlists_paginated(self, page: int, items_per_page: int = 16):
        with self._con as conn:
            curr = conn.cursor()
            curr.execute("""
                       SELECT
                           id,
                           name
                       FROM
                           playlist
                       ORDER BY playlist.id
                       LIMIT (?)
                       OFFSET (?);
                   """, (items_per_page, page * items_per_page))
            results = curr.fetchall()
            if results:
                return tuple(
                    Playlist(
                        id=result[0],
                        name=result[1],
                    ) for result in results)

    def delete_playlist(self, playlist_id: int):
        with self._con as conn:
            cur = conn.cursor()
            cur.execute("""
                      DELETE FROM playlist
                      WHERE id = (?)
                  """, (playlist_id,))
            cur.execute("""
                      DELETE FROM media_playlist
                      WHERE playlist_id = (?)
                  """, (playlist_id,))

    def get_all_categories(self):
        with self._con as conn:
            curr = conn.cursor()
            curr.execute("""
                       SELECT
                           *
                       FROM
                           category
                       ORDER BY category.id;
            """)
            results = curr.fetchall()
            if results:
                return tuple(
                    Category(
                        id=result[0],
                        name=result[1],
                    ) for result in results)

    def insert_media_file_image(self, media_file_id: int, path: str):

        with self._con as conn:
            cur = conn.cursor()

            cur.execute("""
                INSERT INTO image (path)
                VALUES (?);
            """, (str(path),))
            image_id = cur.lastrowid
            cur.execute("""
                UPDATE media_file
                SET image_id = (?)
                WHERE id = (?)
            """, (image_id, media_file_id))

    def get_image_of_media(self, media_file_id: int):

        with self._con as conn:
            cur = conn.cursor()

            cur.execute("""
                SELECT image_id
                FROM media_file
                WHERE id = (?)
            """, (media_file_id,))

            image_id = cur.fetchone()[0]
            cur.execute("""
                SELECT path
                FROM image
                WHERE id = (?);
            """, (image_id,))

            return cur.fetchone()[0]

    def close(self):
        """
        Shutdown method, closes any open connections and stops
        :return:
        """
        self._con.close()


if __name__ == "__main__":
    # Test database creation etc.
    d = Database()
