from pathlib import Path
from typing import Optional

from pydantic import BaseModel

from models.Image import Image


class MediaFile(BaseModel):
    id: int
    name: str
    path: Path
    image: Optional[int]

