import os
import shutil
import zipfile

import aiofiles
import fastapi
from fastapi.responses import FileResponse
from fastapi import UploadFile, File

from database.database import Database

router = fastapi.APIRouter(prefix="/api/dump", tags=["Utility"])
database: Database = Database("data/database.sqlite")

@router.get("")
async def dump() -> FileResponse:
    """
    dump all the data stored into one file.
    """
    path = './dump.zip'
    zip_file = zipfile.ZipFile(path, 'w', zipfile.ZIP_DEFLATED)
    for root, dirs, files in os.walk('./data'):
        for file in files:
            zip_file.write(
                os.path.join(root, file),
                os.path.relpath(os.path.join(root, file), os.path.join('./data', '..'))
            )
    return FileResponse(path=path, filename='dump.txt', media_type="application/zip")


@router.post("")
async def undump(dump: UploadFile = File(...)):
    """
    recieve and load state from file.
    :return:
    """
    async with aiofiles.open('dump.zip', 'w+b') as f:
        await f.write(await dump.read())

    shutil.rmtree('data')
    with zipfile.ZipFile('dump.zip', 'r') as zip_ref:
        zip_ref.extractall('.')
    os.remove('dump.zip')
