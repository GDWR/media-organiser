from enum import Enum
from pydantic import BaseModel


class Category(BaseModel):
    _id: int
    name: str
