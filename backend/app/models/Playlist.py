from pydantic import BaseModel


class Playlist(BaseModel):
    id: int
    name: str
