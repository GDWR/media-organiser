import { Component } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ApiRequestsService, Playlist} from '../../services/api-requests.service';
import {Card} from '../../components/page-layout/page-layout.component';

@Component({
  selector: 'app-playlist-page',
  templateUrl: './playlist-page.page.html',
  styleUrls: ['./playlist-page.page.scss']
})
// tslint:disable-next-line:component-class-suffix
export class PlaylistPage {
  data: Playlist;
  cards: Array<Card>;

  constructor(private route: ActivatedRoute, private api: ApiRequestsService, private router: Router) {

    route.params.pipe().subscribe(
      data => this.api.getPlaylist(data.id).subscribe(
        playlist => {
          this.data = playlist;
          this.cards = [];
          playlist.songs.forEach(
            song => {
              if (song.image) {
                song.image = this.api.imageUrl(song.id);
              }

              this.cards.push({
                id: song.id,
                title: song.name,
                image: song.image || 'assets/default_audio.jpg',
              });
            });
           }
          )
      );
  }

  delete(): void {
    this.api.deletePlaylist(this.data.id).subscribe(
      () => this.router.navigateByUrl('/home')
    );

  }
}
