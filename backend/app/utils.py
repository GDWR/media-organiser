import os


def create_directories():
    """
    Ensure required directories are made.
    """
    try:
        os.mkdir("data")
    except:
        ...
    try:
        os.mkdir("data/files")
    except:
        ...
    try:
        os.mkdir("data/images")
    except:
        ...