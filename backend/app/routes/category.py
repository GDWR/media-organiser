import fastapi
from database.database import Database

router = fastapi.APIRouter(prefix="/api/category", tags=["Utility"])
database: Database = Database("data/database.sqlite")


@router.get("")
async def get_categories():
    """
    Get a list of unique categories
    """
    return database.get_all_categories()
