import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MusicDialogData} from '../add-music-dialog/add-music-dialog.component';

export interface PlaylistDialogData {
  name: string;
}

@Component({
  selector: 'app-add-playlist-dialog',
  templateUrl: './add-playlist-dialog.component.html',
  styleUrls: ['./add-playlist-dialog.component.scss']
})
export class AddPlaylistDialogComponent implements OnInit {

 nameFormControl = new FormControl('', [
      Validators.required,
  ]);
  fileControl =  new FormControl('', [
      Validators.required,
  ]);

  constructor(
    public dialogRef: MatDialogRef<AddPlaylistDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: PlaylistDialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
  }

}
