[![pipeline status](https://gitlab.com/GDWR/media-organiser/badges/master/pipeline.svg)](https://gitlab.com/GDWR/media-organiser/-/commits/master)
# Media Organiser

Install Docker and run with `docker-compose up` while in the root directory




### Database Layout
![Database](examples/database.png "Database")