import fastapi
import uvicorn
from fastapi.middleware.cors import CORSMiddleware
from utils import create_directories

create_directories()


app = fastapi.FastAPI(
    title="Media Organiser",
    description="Backend for Media Organiser. Interacts with a SQL database to store data and uses the "
                "FileSystem to store videos/audio",
    version="1.0.0",
    openapi_tags=[
        {
            "name": "MediaFile",
            "description": "Endpoints for interacting with MediaFiles."
        },
        {
            "name": "Playlist",
            "description": "Endpoints for managing Playlists."
        },
        {
            "name": "Utility",
            "description": "Other small utility endpoints."
        }
    ]
)


def configure():
    # Manually add routes, could be done using glob etc.
    # but would require loading checks for outlets etc.
    from routes.category import router as category_router
    from routes.media_file import router as media_file__router
    from routes.playlist import router as playlist_router
    from routes.tag import router as tag_router
    from routes.dump import router as dump_router

    app.include_router(category_router)
    app.include_router(media_file__router)
    app.include_router(playlist_router)
    app.include_router(tag_router)
    app.include_router(dump_router)


# TODO: Configure properly. https://fastapi.tiangolo.com/tutorial/cors/
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

configure()
if __name__ == "__main__":
    uvicorn.run(app, host="localhost", port=7777)
