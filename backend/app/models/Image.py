from pathlib import Path

from pydantic import BaseModel


class Image(BaseModel):
    _id: int
    path: Path
