import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';


export interface MediaFile {
  id: number;
  name: string;
  path: string;
  image: string;
}

export interface Category {
  id: number;
  name: string;
}

export interface Tag {
  id: number;
  name: string;
}

export interface Playlist {
  id: number;
  name: string;
  songs: Array<MediaFile> | null;
}


@Injectable({
  providedIn: 'root'
})
export class ApiRequestsService {

  constructor(private http: HttpClient) { }
  apiBase = 'http://localhost:7777/api';
  mediaFileEndpoint = `${this.apiBase}/media_file`;
  playlistEndpoint = `${this.apiBase}/playlist`;
  tagEndpoint = `${this.apiBase}/tag`;
  categoryEndpoint = `${this.apiBase}/category`;

  headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*'
    });

  get allCategories(): Observable<Category[]> {
    return this.http.get<Category[]>(this.categoryEndpoint, {headers: this.headers});
  }

  get allTags(): Observable<Tag[]> {
    return this.http.get<Tag[]>(this.tagEndpoint, {headers: this.headers});
  }

  getPlaylistPage(page: number, itemsPerPage: number): Observable<Playlist[]> {
    const params = new HttpParams()
      .set('page', page.toString())
      .set('items_per_page', itemsPerPage.toString());

    return this.http.get<Playlist[]>(this.playlistEndpoint, {headers: this.headers, params});
  }

  addNewPlaylist(name: string): Observable<Playlist> {
    const params = new HttpParams()
      .set('name', name.toString());

    return this.http.post<Playlist>(this.playlistEndpoint, '', {headers: this.headers, params});
  }

  deletePlaylist(playlistId: number): Observable<boolean> {
    const params = new HttpParams()
      .set('playlist_id', playlistId.toString());

    return this.http.delete<boolean>(this.playlistEndpoint, {headers: this.headers, params});
  }

  getPlaylist(playlistId: number): Observable<Playlist> {
    return this.http.get<Playlist>(`${this.playlistEndpoint}/${playlistId}`, {headers: this.headers});
  }

  addToPlaylist(playlistId: number, mediaFileId: number): Observable<boolean> {
    const params = new HttpParams()
      .set('media_file_id', mediaFileId.toString());

    return this.http.post<boolean>(
      `${this.playlistEndpoint}/${playlistId}`,
      '',
      {headers: this.headers, params});
  }

  removeFromPlaylist(playlistId: number, mediaFileId: number): Observable<boolean> {
    const params = new HttpParams()
      .set('media_file_id', mediaFileId.toString());

    return this.http.delete<boolean>(
      `${this.playlistEndpoint}/${playlistId}`,
      {headers: this.headers, params});
  }

  getAllMediaFiles(pageNumber: number, itemsPerPage: number): Observable<MediaFile[]>{
    const params = new HttpParams()
      .set('page', pageNumber.toString())
      .set('items_per_page', itemsPerPage.toString());

    return this.http.get<MediaFile[]>(this.mediaFileEndpoint, {headers: this.headers, params});
  }

  uploadMediaFile(name: string, file: File): Observable<boolean> {
    const params = new HttpParams()
      .set('name', name);

    const formData = new FormData();
    formData.append('file', file, file.name);

    return this.http.post<boolean>(this.mediaFileEndpoint, formData, {headers: this.headers, params});
  }

  getMediaFileCategories(mediaFileId: number): Observable<Category[]> {
    return this.http.get<Category[]>(
      `${this.mediaFileEndpoint}/${mediaFileId}/category`,
      {headers: this.headers}
      );
  }

  addMediaFileCategory(mediaFileId: number, categoryId: number): Observable<boolean> {
    const params = new HttpParams()
      .set('category_id', categoryId.toString());

    return this.http.post<boolean>(`${this.mediaFileEndpoint}/${mediaFileId}/category`,
      '',
      {headers: this.headers, params}
      );
  }

  getMediaFileTags(mediaFileId: number): Observable<Tag[]> {
    return this.http.get<Tag[]>(`${this.mediaFileEndpoint}/${mediaFileId}/tag`,
      {headers: this.headers}
      );
  }

  addMediaFileTag(mediaFileId: number, tag: string): Observable<boolean> {
    const params = new HttpParams()
      .set('tag', tag);

    return this.http.post<boolean>(`${this.mediaFileEndpoint}/${mediaFileId}/tag`,
      '',
      {headers: this.headers, params}
      );
  }

  getMediaFile(mediaFileId: number): Observable<MediaFile> {
    return this.http.get<MediaFile>(`${this.mediaFileEndpoint}/${mediaFileId}`, {headers: this.headers});
  }

  deleteMediaFile(mediaFileId: number): Observable<boolean> {
    return this.http.delete<boolean>(`${this.mediaFileEndpoint}/${mediaFileId}`, {headers: this.headers});
  }

  getMediaFilePlaylists(mediaFileId: number): Observable<Playlist[]> {
    return this.http.get<Playlist[]>(`${this.mediaFileEndpoint}/${mediaFileId}/playlist`, {headers: this.headers});
  }

  getMediaFileStreamUrl(mediaFileId: number): string {
    return `${this.mediaFileEndpoint}/${mediaFileId}/stream`;
  }

  getMediaFileDownloadUrl(mediaFileId: number): string {
    return `${this.mediaFileEndpoint}/${mediaFileId}/download`;
  }

  addImageToMediaFile(mediaFileId: number, file: File): Observable<boolean> {
    const formData = new FormData();
    formData.append('file', file, file.name);

    return this.http.post<boolean>(`${this.mediaFileEndpoint}/${mediaFileId}/image`, formData, {headers: this.headers});
  }

  imageUrl(id: number): string {
    return `${this.mediaFileEndpoint}/${id}/image`;
  }
}
