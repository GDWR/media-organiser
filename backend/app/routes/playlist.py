import fastapi
from fastapi import HTTPException

from database.database import Database

router = fastapi.APIRouter(prefix="/api/playlist", tags=["Playlist"])
database: Database = Database("data/database.sqlite")


@router.post("")
async def add_new_playlist(name: str):
    """
    Create a new playlist
    """
    database.insert_playlist(name)


@router.post("/{playlist_id}")
async def add_media_to_playlist(playlist_id: int, media_file_id: int):
    """
    Add a MediaFile to a existing playlist
    """
    database.add_media_file_to_playlist(playlist_id, media_file_id)


@router.get("")
async def get_paginated_playlist(page: int, items_per_page: int):
    """
    Get a list of playlists, paginated.
    """
    return database.get_playlists_paginated(page, items_per_page)


@router.get("/{playlist_id}")
async def get_playlist_by_id(playlist_id: int):
    """
    Get a playlist via ID
    """
    playlist = database.get_playlist(playlist_id)
    return {
        "id": playlist.id,
        "name": playlist.name,
        "songs": database.get_media_files_from_playlist(playlist_id)
    }


@router.delete("/{playlist_id}")
async def remove_media_from_playlist(playlist_id: int, media_file_id: int):
    """
    Delete a MediaFile from a playlist
    """
    raise HTTPException(status_code=500, detail="Not implemented")


@router.delete("")
async def delete_playlist(playlist_id: int):
    """
    Delete a playlist
    """
    database.delete_playlist(playlist_id)
