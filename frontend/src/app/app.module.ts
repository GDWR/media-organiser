import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LayoutModule} from '@angular/cdk/layout';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatCardModule} from '@angular/material/card';
import {MatMenuModule} from '@angular/material/menu';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {HttpClientModule} from '@angular/common/http';
import {NgxSkeletonLoaderModule} from 'ngx-skeleton-loader';
import {MatChipsModule} from '@angular/material/chips';
import {MatDialogModule} from '@angular/material/dialog';
import {MatPaginatorModule} from '@angular/material/paginator';
import {AddMusicDialogComponent} from './dialog/add-music-dialog/add-music-dialog.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {NgxMatFileInputModule} from '@angular-material-components/file-input';
import {AddTagDialogComponent} from './dialog/add-tag-dialog/add-tag-dialog.component';
import {MusicPage} from './pages/music-page/music-page.page';
import {PlaylistPage} from './pages/playlist-page/playlist-page.page';
import {CategoryPage} from './pages/category-page/category-page.page';
import {AppRoutingModule} from './app-routing.module';
import {PageNotFoundComponent} from './pages/page-not-found/page-not-found.component';
import {AddPlaylistDialogComponent} from './dialog/add-playlist-dialog/add-playlist-dialog.component';
import {FlexModule, GridModule} from '@angular/flex-layout';
import {MatTableModule} from '@angular/material/table';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {SideDrawerComponent} from './components/side-drawer/side-drawer.component';
import {PageLayoutComponent} from './components/page-layout/page-layout.component';
import {MediaPlayerComponent} from './components/media-player/media-player.component';
import {MatSliderModule} from '@angular/material/slider';
import {HomePageComponent} from './pages/home-page/home-page.component';
import { AddImageDialogComponent } from './dialog/add-image-dialog/add-image-dialog.component';
import { AddCategoryDialogComponent } from './dialog/add-category-dialog/add-category-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    AddMusicDialogComponent,
    AddTagDialogComponent,
    MusicPage,
    PlaylistPage,
    CategoryPage,
    PageNotFoundComponent,
    AddPlaylistDialogComponent,
    SideDrawerComponent,
    PageLayoutComponent,
    MediaPlayerComponent,
    HomePageComponent,
    AddImageDialogComponent,
    AddCategoryDialogComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    DragDropModule,
    HttpClientModule,
    NgxSkeletonLoaderModule,
    MatChipsModule,
    MatDialogModule,
    MatPaginatorModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    NgxMatFileInputModule,
    AppRoutingModule,
    FlexModule,
    MatTableModule,
    GridModule,
    MatSliderModule,
    MatProgressSpinnerModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
}
