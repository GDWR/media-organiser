import { Component, OnInit } from '@angular/core';

export interface CategoryCard {
  name: string;
  colour: string;
  image: string;
}


@Component({
  selector: 'app-category-page',
  templateUrl: './category-page.page.html',
  styleUrls: ['./category-page.page.scss']
})
// tslint:disable-next-line:component-class-suffix
export class CategoryPage implements OnInit {
  cards: Array<CategoryCard> = [
    {name: 'Pop', colour: '#ED0A3F', image: './assets/pop.jpg'},
    {name: 'Rock', colour: '#FF8833', image: './assets/rock.jpg'},
    {name: 'Electronic', colour: '#FFFF99', image: './assets/electronic.jpg'},
    {name: 'RnB', colour: '#7BA05B', image: './assets/rnb.jpg'},
    {name: 'Funk', colour: '#01A638', image: './assets/funk.jpg'},
    {name: 'Country', colour: '#29AB87', image: './assets/country.jpg'},
    {name: 'Latin', colour: '#8FD8D8', image: './assets/latin.jpg'},
    {name: 'HipHop', colour: '#003366', image: './assets/hiphop.jpg'},
    {name: 'Punk', colour: '#6356B7', image: './assets/punk.jpg'},
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
