import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {AudioPlayerService} from '../../services/audio-player.service';
import {MatSlider} from '@angular/material/slider';

@Component({
  selector: 'app-media-player',
  templateUrl: './media-player.component.html',
  styleUrls: ['./media-player.component.scss']
})
export class MediaPlayerComponent {
  @ViewChild('slider') slider: MatSlider;
  audio = new Audio();

  constructor(audioPlayer: AudioPlayerService) {
    audioPlayer.listenForRequests().subscribe(
      request => this.play(request.name, request.url)
    );

    this.audio.addEventListener('',
      () => console.log('')
    );
  }

  pause(): void {
    this.audio.pause();
  }

  resume(): void {
    this.audio.play();
  }

  play(name: string, url: string): void {
    this.audio.src = url;
    this.audio.play();
  }

  changeTime(event): void {
    // this.audio.currentTime = event.value;
  }


}
