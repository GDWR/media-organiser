export function delay(ms: number): Promise<any> {
  return new Promise( resolve => setTimeout(resolve, ms) );
}

export function categoryToColour(name: string): string {
    switch (name){
      case 'Pop': // Pop
        return '#ED0A3F';
      case 'Rock': // Rock
        return '#FF8833';
      case 'Electronic': // Electronic
        return '#FFFF99';
      case 'RnB': // RnB
        return '#7BA05B';
      case 'Funk': // Funk
        return '#01A638';
      case 'Country': // Country
        return '#29AB87';
      case 'Latin': // Latin
        return '#8FD8D8';
      case 'HipHop': // HipHop
        return '#003366';
      case 'Punk': // Punk
        return '#6456B7';
      case 'Rap': // Rap
        return '#FC74FD';
      default:
        return '#787878';
    }
  }
