import { Component, OnInit} from '@angular/core';
import { Card } from '../../components/page-layout/page-layout.component';

@Component({
  selector: 'app-music-page',
  templateUrl: './music-page.page.html',
  styleUrls: ['./music-page.page.scss'],

})
// tslint:disable-next-line:component-class-suffix
export class MusicPage implements OnInit {
  cards: Array<Card> = [
    {id: 1, title: 'Test', image: 'assets/default_audio.jpg'},
    {id: 1, title: 'Test', image: 'assets/default_audio.jpg'},
    {id: 1, title: 'Test', image: 'assets/default_audio.jpg'},
    {id: 1, title: 'Test', image: 'assets/default_audio.jpg'},
    {id: 1, title: 'Test', image: 'assets/default_audio.jpg'},
    {id: 1, title: 'Test', image: 'assets/default_audio.jpg'},
    {id: 1, title: 'Test', image: 'assets/default_audio.jpg'},
    {id: 1, title: 'Test', image: 'assets/default_audio.jpg'},
    {id: 1, title: 'Test', image: 'assets/default_audio.jpg'},
    {id: 1, title: 'Test', image: 'assets/default_audio.jpg'},
    {id: 1, title: 'Test', image: 'assets/default_audio.jpg'},
  ];


  constructor() { }

  ngOnInit(): void {
  }

}
