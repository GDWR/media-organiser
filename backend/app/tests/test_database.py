import random
from pathlib import Path
from sqlite3 import Cursor, IntegrityError

import pytest

from database.database import Database
from models.Category import Category


@pytest.fixture(scope="session")
def database() -> Database:
    # Database is done in memory so it is lost after testing.
    database = Database(":memory:")
    yield database
    database.close()


@pytest.fixture(scope="function")
def cursor(database) -> Cursor:
    cur = database._con.cursor()
    yield cur
    cur.close()


def test_tables_exist(cursor):
    """
    Test that all tables were created when the database is initialised.
    :param cursor:
    :return:
    """
    cursor.execute("""
        SELECT name
        FROM sqlite_master
        WHERE type='table';
    """)
    expected_tables = [
        "image", "media_file",
        "playlist", "media_playlist",
        "tag", "media_tag",
        "category", "media_category"
    ]
    # Comes a 1 item tuple, this unpacks it.
    results = (result[0] for result in cursor.fetchall())

    for table in expected_tables:
        assert table in results, "Table was not created!"


def test_database_doesnt_make_duplicate_tables(database: Database):
    """
    Ensure that tables don't make duplicates or throw errors.
    :param database:
    :return:
    """
    try:
        database._ensure_created()
    except:
        assert False, "Database failed to ensure the tables were created after a second run."


def test_add_media_files(database: Database):
    """
    Tests creating a media file in the database, Then tries to retrieve it to ensure the data is the same.
    :param database:
    :return:
    """
    media_files = []

    for i in range(1, 51):
        try:
            entry = database.insert_media_file(f"ExampleFile{i}", Path(f"tests/example{i}.mp3"))
            media_files.append(entry)

        except IntegrityError:
            ...

    for media_file in media_files:
        entry = database.get_media_file(media_file.id)
        assert entry, "Entry wasn't found!"
        assert entry.id == media_file.id, "Id was not the same"
        assert entry.name == media_file.name, "File Name was not the same"
        assert entry.path == media_file.path, "Path was not the same"
        assert entry.image == media_file.image, "Image was not the same"


def test_get_all_media_files(database: Database):
    """
    Check that media files can be retrieved from the database.
    """
    items_per_page = 8
    page = database.get_all_media_files(0, items_per_page)
    item_count = 0
    for file in page:
        item_count += 1

    assert item_count <= items_per_page, "Incorrect number of items per page"

    assert item_count == 8, "Missing files or Too many Files"


def test_add_and_get_tag_to_media_file(database: Database):
    page = database.get_all_media_files(0)
    word_list = ["Test", "Words", "Have", "No", "Meaning"]

    for media_file in page:
        tag = random.choice(word_list)
        database.add_tag_to_media_file(tag, media_file)

        tags = database.get_tags_of_media_file(media_file)
        assert all(tag.name in word_list for tag in tags), "Invalid Category"


def test_playlist(database: Database):
    playlist = database.insert_playlist("TestPlaylist")
    assert playlist, "Playlist wasn't created"
    test_ids = [32, 12, 42, 1, 39]
    for i in test_ids:
        media_file = database.get_media_file(i)
        database.add_media_file_to_playlist(playlist, media_file)
        for file in database.get_media_files_from_playlist(i):
            assert file.id in test_ids, "Incorrect file"

