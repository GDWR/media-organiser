import fastapi

router = fastapi.APIRouter(prefix="/api/tag", tags=["Utility"])


@router.get("")
async def get_tag():
    """
    Get a list of all unique tags.
    """
    ...