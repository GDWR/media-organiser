import {Injectable, OnChanges, SimpleChanges} from '@angular/core';
import {BehaviorSubject, Observable, Subscriber} from 'rxjs';

export interface Request{
  name: string;
  url: string;
}


@Injectable({
  providedIn: 'root'
})
export class AudioPlayerService {
  requestListener: BehaviorSubject<Request>;


  constructor() {
    this.requestListener = new BehaviorSubject<Request>({name: '', url: ''});
  }

  songRequest(name: string, url: string): void {
    this.requestListener.next({name, url});
  }

  listenForRequests(): Observable<Request> {
    return this.requestListener.asObservable();
  }
}
