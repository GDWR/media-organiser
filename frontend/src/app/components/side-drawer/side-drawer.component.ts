import {Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges} from '@angular/core';
import {ApiRequestsService} from '../../services/api-requests.service';
import {MatDialog} from '@angular/material/dialog';
import {AddMusicDialogComponent} from '../../dialog/add-music-dialog/add-music-dialog.component';
import {Observable, Subscription} from "rxjs";
import {AddPlaylistDialogComponent} from "../../dialog/add-playlist-dialog/add-playlist-dialog.component";

interface Playlist {
  id: number;
  name: string;
}

@Component({
  selector: 'app-side-drawer',
  templateUrl: './side-drawer.component.html',
  styleUrls: ['./side-drawer.component.scss']
})
export class SideDrawerComponent implements OnInit {
  playlists: Array<Playlist>;


  constructor(private api: ApiRequestsService, public dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.reload();
  }

  reload(): void {
    this.api.getPlaylistPage(0, 8).subscribe(
      playlists => {
        this.playlists = [];
        this.playlists = playlists;
      }
    );
  }

  uploadFiles(): void {
    const dialogRef = this.dialog.open(AddMusicDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      this.api.uploadMediaFile(result.name, result.file).subscribe(
        () => this.reload()
      );
    });
  }

  getLink(id: number): string {
    return `/playlist/${id}`;
  }

  addPlaylist(): void {
    const dialogRef = this.dialog.open(AddPlaylistDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      this.api.addNewPlaylist(result.name).subscribe(
        () => this.reload()
      );
    });
  }
}
