import { Component, OnInit } from '@angular/core';
import {Card} from '../../components/page-layout/page-layout.component';
import {ApiRequestsService} from "../../services/api-requests.service";

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  cards: Array<Card> = [];

  constructor(private api: ApiRequestsService) { }

  ngOnInit(): void {
    this.api.getAllMediaFiles(0, 16).subscribe(
  mediaFiles => mediaFiles.forEach(
          file => this.cards.push({
              id: file.id,
              title: file.name,
              image: file.image || 'assets/default_audio.jpg',
            })
          ));
  }

}
