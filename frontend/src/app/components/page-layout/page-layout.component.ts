import {Component, Input, OnChanges, OnInit, SimpleChanges, EventEmitter, Output} from '@angular/core';
import {ApiRequestsService, Category, Playlist} from '../../services/api-requests.service';
import {AudioPlayerService} from '../../services/audio-player.service';
import {MatDialog} from '@angular/material/dialog';
import {AddImageDialogComponent} from '../../dialog/add-image-dialog/add-image-dialog.component';
import {AddCategoryDialogComponent} from '../../dialog/add-category-dialog/add-category-dialog.component';

export interface Card {
  id: number;
  title: string;
  image: string;
}


@Component({
  selector: 'app-page-layout',
  templateUrl: './page-layout.component.html',
  styleUrls: ['./page-layout.component.scss']
})
export class PageLayoutComponent implements OnInit, OnChanges {
  @Input() cards: Array<Card>;
  @Output() reloadEvent: EventEmitter<void>;
  playlists: Array<Playlist> = [];
  categories: Array<Category> = [];

  constructor(private api: ApiRequestsService, private audio: AudioPlayerService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.api.getPlaylistPage(0, 24).subscribe(
      playlist => this.playlists = playlist
    );
    this.api.allCategories.subscribe(
      categories => this.categories = categories
    );
  }

  ngOnChanges(changes: SimpleChanges): void {

  }

  reload(): void {

  }

  imageDialog(mediaFileId: number): void {
    const dialogRef = this.dialog.open(AddImageDialogComponent);

    dialogRef.afterClosed().subscribe(
      result => this.api.addImageToMediaFile(mediaFileId, result.file).subscribe(
        () => this.reload()
      )
    );
  }

  delete(cardId: number): void {
    this.api.deleteMediaFile(cardId).subscribe(
      () => this.reload()
    );
  }

  addCategory(cardId: number, categoryId: number): void {
     this.api.addMediaFileCategory(cardId, categoryId).subscribe(
      () => this.reload()
    );

  }

  addToPlaylist(cardId: number, playlistId: number): void {
    this.api.addToPlaylist(playlistId, cardId).subscribe(
      () => this.reload()
    );
  }

  play(card: Card): void {
    this.audio.songRequest(card.title, this.api.getMediaFileStreamUrl(card.id));
  }

  newCategory(): void {
    const dialogRef = this.dialog.open(AddCategoryDialogComponent);

    dialogRef.afterClosed().subscribe(result => console.log(result));
  }
}
